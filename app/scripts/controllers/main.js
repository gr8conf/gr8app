app.controller('MainCtrl', ['$scope', '$routeParams', 'scrollService', 'AuthService', 'modalService','FEATURES',  function ($scope, $routeParams, scrollService, AuthService, modalService, FEATURES) {
    $scope.FEATURES = FEATURES;
    $scope.loginEnabled = FEATURES.login;
    $scope.$watch(AuthService.isLoggedIn, function (isLoggedIn) {
        $scope.isLoggedIn = isLoggedIn;
        $scope.currentUser = AuthService.currentUser();
    });
    $scope.login = modalService.login;
    $scope.logout = modalService.logout;
    $scope.showPriceInfo = false;
    if($routeParams.anchor) {
        setTimeout(function() {
            scrollService.scrollTo($routeParams.anchor, "AnchorScroll",$routeParams.anchor);
        }, 2000);
    }
}]);