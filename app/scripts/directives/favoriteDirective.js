'use strict';

angular.module('gr8appApp')
    .directive('favorite', ['backendService','FEATURES', function (backendService, FEATURES) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/partials/directives/_favorite.html',
            scope: {
                id: '='
            },
            controller: ['$scope', '$rootScope', 'AuthService', function ($scope, $rootScope, AuthService) {
                var id = $scope.id;
                $scope.isEnabled = FEATURES.favorites;
                $scope.isLoggedIn = AuthService.isLoggedIn();
                $scope.favorited = AuthService.isLoggedIn() ? backendService.isFavorite(id) : false;

                $scope.toggleFavorite = function (event) {
                    event.stopPropagation();
                    if (AuthService.isLoggedIn()) {
                        $scope.favorited = !$scope.favorited;
                        $rootScope.$broadcast('favorited', [id, $scope.favorited]);
                    }
                };

                $scope.$watch('favorited', function () {
                    $scope.icon = AuthService.isLoggedIn() ? ($scope.favorited ? 'selected' : 'unselected') : 'disabled';
                    $scope.tooltip = AuthService.isLoggedIn() ? ($scope.favorited ? 'Click to un-favorite' : 'Click to favorite') : 'Login in to use favorites';

                });

                $rootScope.$on('favorited', function(event,args){
                    var eventId = args[0];
                    var eventFavorited = args[1];
                    if(eventId === id) {
                        $scope.favorited = eventFavorited;
                    }
                });

                var changedFavorites = function (event, args) {
                    if (args[0] === 'favorites') {
                        $scope.favorited = AuthService.isLoggedIn() ? backendService.isFavorite(id) : false;
                        $scope.icon = AuthService.isLoggedIn() ? ($scope.favorited ? 'selected' : 'unselected') : 'disabled';
                        $scope.tooltip = AuthService.isLoggedIn() ? ($scope.favorited ? 'Click to un-favorite' : 'Click to favorite') : 'Login in to use favorites';
                    }
                };
                $rootScope.$on('loaded', changedFavorites);
                $rootScope.$on('saved', changedFavorites);
                $rootScope.$on('login', function () {
                    $scope.favorited =  backendService.isFavorite(id);
                });
                $rootScope.$on('logout', function () {
                    $scope.isLoggedIn = false;
                    $scope.icon = 'disabled';
                    $scope.tooltip = "Login to use favorites";
                });

            }]
        };
    }]);
