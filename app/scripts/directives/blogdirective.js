angular.module('gr8appApp')
.directive('blog', function() {
    return {
        restrict: "E",
        replace: false,
        templateUrl: 'views/partials/_blogEntry.html',
        scope: {
            post: '='
        },
        controller: ['$scope','modalService',function($scope, modalService) {
            $scope.readBlog = function(id) {
                modalService.showBlog(id)
            };

            $scope.formatDate = function(date) {
                return moment(date).format('YYYY-MM-DD');
            };

        }]
    };
});
