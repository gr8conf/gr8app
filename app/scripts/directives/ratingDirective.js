'use strict';

app
    .directive('gr8rating', [ function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/partials/directives/_rating.html',
            scope: {
                id: '='
            },
            controller: ['$scope', '$rootScope', 'backendService', 'FEATURES', 'AuthService','modalService', function ($scope, $rootScope, backendService, FEATURES, AuthService, modalService) {
                var id = $scope.id;
                $scope.isEnabled = FEATURES.ratings;
                $scope.isLoggedIn = AuthService.isLoggedIn();
                $scope.rated = AuthService.isLoggedIn() ? backendService.getRating(id) : {};

                $scope.openRating = function(event) {
                    event.stopPropagation();
                    if (AuthService.isLoggedIn()) {
                        modalService.editRating(id)
                    }
                };

                var changedRating = function (event, args) {
                    if (args[0] === 'ratings') {
                        $scope.rated = AuthService.isLoggedIn() ? backendService.getRating(id) : {};
                        updateIcons();
                    }
                };
                var updateIcons = function() {
                    var rated = $scope.rated && $scope.rated.id;
                    $scope.icon = AuthService.isLoggedIn() ? (rated ? 'rated' : 'unrated') : 'disabled';
                    $scope.tooltip = AuthService.isLoggedIn() ? (rated ? 'Click to change rating': 'Click to rate') : 'Login in to use rating';
                };
                $scope.$watch('rated', updateIcons);

                $rootScope.$on('loaded', changedRating);
                $rootScope.$on('saved', changedRating);
                $rootScope.$on('login', function () {
                    $scope.rated = backendService.getRating(id);
                });
                $rootScope.$on('logout', function () {
                    $scope.isLoggedIn = false;
                    updateIcons();
                });

            }]
        };
    }]);
